# Standard Template Library (STL)

# Headers

<cstdlib>
	program control			  - Appliction
	dynamic memory allocation - Memory namespace
	random numbers			  -	Math::Random
	sort and search			  - Not sure
	Numeric string conversion - Not sure
<csignal> - Not important
<csetjmp> - Not important
<cstdarg>
	Variadic Functions - Need create API for this
<typeinfo>
	Type support - DataTypes
	basic types - DataTypes
	type_info - DataTypes::TypeInfo
		need create this class and create new function
		in IObject that return this class info
<typeindex> - Not important
<type_traits> - Not important
<bitset> - In DataTypes::Collections
<functional> - DataTypes::Function (FunctionObject)
<utility>
	Swap and type operations - Need to create
	Date and time - Time / DataTypes
	Formatting library - DataTypes::String
<ctime> - Time / DataTypes
<chrono> - Time / DataTypes
<cstddef> - Not important
<initializer_list> - Not important
<tuple> - Not important
<any> - Not important
<optional> - Not important
<variant> - Not important
<compare> - Not sure
<version> - Updates::Version
<source_location> - Debug namespace
<stacktrace> - Debug namespace

<new> - Memory namespace
<memory>
	Smart pointers - DataTypes
	Allocators (Dynamic memory) - Memory namespace
	Memory resources - Memory namespace, MemoryManagment
<scoped_allocator> - Not sure
<memory_resource> - Memory namespace

<climits> - Not important
<cfloat> - Not important
<cstdint> - Not important
<cinttypes> - Not important

<exception> - Need create System for this
<stdexcept> - Need create System for this
<cassert> - Not important
<system_error> - Need create System for this
<cerrno> - Not important

<cctype> - DataTypes::String
<cwctype> - DataTypes::String
<cstring> - DataTypes::String
<cwchar> - DataTypes::String
<cuchar> - DataTypes::String
<string> - DataTypes::String
<string_view> - DataTypes::String
<charconv> - DataTypes::String
<format> - DataTypes::String

<array> - DataTypes::Collections::Array
<vector> - DataTypes::Collections::DynamicArray
<deque> - Not sure
<list> - DataTypes::Collections::List
<forward_list> - Not sure
<set> - Not sure
<map> - Not sure
<unordered_set> - Not sure
<unordered_map> - Not sure
<stack> - DataTypes::Collections::Stack
<queue> - DataTypes::Collections::Queue
<span> - Not sure

<iterator> - DataTypes::Iterators

<ranges> - Not sure

<algorithm>
	Need to learn more deeply
<execution>  - Not important

<cmath> - Math
<complex> - Math / DataTypes
<valarray> - Not sure
<random> - Math::Random
<numeric> - Not sure
<ratio> - Not sure
<cfenv> - Not sure
<bit> - Math / DataTypes
<numbers> - Math::Constants
	PI, E (Not physics constants)

<locale> - Time / DataTypes
<clocale> - Time / DataTypes
<codecvt> - Time / DataTypes

...