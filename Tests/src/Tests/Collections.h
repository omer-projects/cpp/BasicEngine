#pragma once

#include "Debug.h"
#include "TestObject.h"

#include "Engine/Core.h"
#include "Engine/DataTypes/Collections/Array.h"
#include "Engine/DataTypes/Collections/DynamicArray.h"
#include "Engine/DataTypes/Collections/Stack.h"
#include "Engine/DataTypes/Collections/Queue.h"
#include "Engine/DataTypes/Collections/Deque.h"
#include "Engine/DataTypes/Collections/List.h"
#include "Engine/DataTypes/Collections/CircularLinkedList.h"
#include "Engine/DataTypes/Collections/BinarySearchTree.h"

using namespace Engine;
using namespace Engine::DataTypes::Collections;

using Obj = TestObject;

namespace Tests::Collections
{
	void TestArray()
	{
		Engine::Array<Obj, 5> arr;
		for (uint i = 0; i < arr.GetLength(); i++)
		{
			arr.Set(i, Obj(i));
		}

		for (uint i = 0; i < arr.GetLength(); i++)
		{
			INFO_LOG(arr[i].m_Value);
		}

		INFO_LOG("End Test");
	}

	void TestDynamicArray()
	{
		DynamicArray<Obj> arr;

		for (uint i = 0; i < 5; i++)
		{
			arr.Push(Obj(i));
		}

		for (uint i = 0; i < arr.GetLength(); i++)
		{
			INFO_LOG(arr[i].m_Value);
		}

		INFO_LOG("End Test");
	}

	void TestStack()
	{
		Stack<Obj> stack;
		if (stack.HasNodes())
		{
			INFO_LOG("Has Nodes (1)");
		}

		Obj number = stack.Pull();
		INFO_LOG(number.m_Value);

		stack.Push(Obj(2));

		INFO_LOG("Get()");
		INFO_LOG(stack.Get()->m_Value);

		stack.Push(Obj(3));
		stack.Push(Obj(5));
		stack.Push(Obj(7));
		stack.Push(Obj(11));

		if (stack.HasNodes())
		{
			INFO_LOG("Has Nodes (2)");
		}

		while (stack.HasNodes())
		{
			number = stack.Pull();
			if (number.m_Value > 1)
			{
				INFO_LOG(number.m_Value);
				stack.Push(Obj(number.m_Value / 2));
			}
		}

		INFO_LOG("End Test");
	}

	void TestQueue()
	{
		Queue<Obj> queue;
		if (queue.HasNodes())
		{
			INFO_LOG("Has Nodes (1)");
		}

		Obj number = queue.Pull();
		INFO_LOG(number.m_Value);

		queue.Push(Obj(2));

		INFO_LOG("Get()");
		INFO_LOG(queue.Get()->m_Value);

		queue.Push(Obj(3));
		queue.Push(Obj(5));
		queue.Push(Obj(7));
		queue.Push(Obj(11));

		if (queue.HasNodes())
		{
			INFO_LOG("Has Nodes (2)");
		}

		while (queue.HasNodes())
		{
			number = queue.Pull();
			INFO_LOG(number.m_Value);
		}

		INFO_LOG("End Test");
	}

	void TestDeque()
	{
		Deque<Obj> deque;
		if (deque.HasNodes())
		{
			INFO_LOG("Has Nodes (1)");
		}

		Obj number = deque.PullFromTop();
		INFO_LOG(number.m_Value);

		deque.PushToTop(Obj(2));

		INFO_LOG("Get()");
		INFO_LOG(deque.GetTop()->m_Value);

		deque.PushToTop(Obj(3));
		deque.PushToTop(Obj(5));
		deque.PushToTop(Obj(7));
		deque.PushToTop(Obj(11));

		if (deque.HasNodes())
		{
			INFO_LOG("Has Nodes (2)");
		}

		while (deque.HasNodes())
		{
			number = deque.PullFromTop();
			INFO_LOG(number.m_Value);
		}

		INFO_LOG("End Test");
	}

	void TestList()
	{
		Engine::List<Obj> list;
		if (list.HasNodes())
		{
			INFO_LOG("Has Nodes (1)");
		}

		Obj number;

		list.Add(Obj(2));
		list.Add(Obj(3));
		list.Add(Obj(5));
		list.Add(Obj(7));
		list.Add(Obj(11));

		if (list.HasNodes())
		{
			INFO_LOG("Has Nodes (2)");
		}

		for (Engine::uint i = 0; i < list.GetLength(); i++)
		{
			number = *list.Get(i);
			INFO_LOG(number.m_Value);
		}

		INFO_LOG("remove");

		while (list.HasNodes())
		{
			number = *list.Get(0);
			list.Remove(0);
			INFO_LOG(number.m_Value);
		}

		INFO_LOG("Recreate the list");

		list.Add(Obj(2));
		list.Add(Obj(3));
		list.Add(Obj(5));
		list.Add(Obj(7));
		list.Add(Obj(11));

		for (Engine::uint i = 0; i < list.GetLength(); i++)
		{
			number = *list.Get(i);
			INFO_LOG(number.m_Value);
		}

		INFO_LOG("remove count");

		list.Remove(list.GetLength() - 2, 2);

		for (Engine::uint i = 0; i < list.GetLength(); i++)
		{
			number = *list.Get(i);
			INFO_LOG(number.m_Value);
		}

		list.Clear();

		if (list.HasNodes())
		{
			INFO_LOG("Has Nodes (3)");
		}

		INFO_LOG("End Test");
	}

	void TestCircularLinkedList()
	{
		CircularLinkedList<Obj> linkedList;
		if (linkedList.HasNodes())
		{
			INFO_LOG("Has Nodes (1)");
		}

		Obj number = linkedList.Pull();
		INFO_LOG(number.m_Value);

		linkedList.Push(Obj(2));

		INFO_LOG("Get()");
		INFO_LOG(linkedList.Get()->m_Value);

		linkedList.Push(Obj(3));
		linkedList.Push(Obj(5));
		linkedList.Push(Obj(7));
		linkedList.Push(Obj(11));

		if (linkedList.HasNodes())
		{
			INFO_LOG("Has Nodes (2)");
		}

		int first = linkedList.Get()->m_Value;
		INFO_LOG(first);
		linkedList++;

		while (linkedList.Get()->m_Value != first)
		{
			INFO_LOG(linkedList.Get()->m_Value);
			linkedList++;
		}

		linkedList--;
		while (linkedList.Get()->m_Value != first)
		{
			INFO_LOG(linkedList.Get()->m_Value);
			linkedList--;
		}

		INFO_LOG("pull all");

		while (linkedList.HasNodes())
		{
			INFO_LOG(linkedList.Pull().m_Value);
		}

		INFO_LOG("End Test");
	}

	void TestBinarySearchTree()
	{
		BinarySearchTree<int> tree;

		tree.Push(7);
		tree.Push(3);
		tree.Push(11);
		tree.Push(1);
		tree.Push(5);
		tree.Push(9);
		tree.Push(13);
		tree.Push(4);
		tree.Push(6);
		tree.Push(8);
		tree.Push(12);
		tree.Push(14);

		tree.Print();

		INFO_LOG("End Test");
	}
}