#pragma once

#include "Debug.h"

#define MEMORY_LOG(...) if (s_ShowMemoryInfo) { LOG("Memory", __VA_ARGS__); }

static bool s_ShowMemoryInfo = true;

namespace Debug
{
    void ShowMemoryInfo(bool value)
    {
        s_ShowMemoryInfo = value;
    }
}

void* operator new(size_t size)
{
    MEMORY_LOG("Malloc: ", size, " bytes");
    
    void* pointer = malloc(size);
    return pointer;
}

void operator delete(void* pointer, std::size_t size)
{
    MEMORY_LOG("Free: ", size, " bytes");

    free(pointer);
}