#pragma once

#include <string>
#include <iostream>

#define LOG(CATEGORY, ...) Debug::Logger::Info(CATEGORY, __VA_ARGS__)

#if INFO_CATEGORY == 1
	#define INFO_LOG(...) LOG("INFO", __VA_ARGS__)
#else
	#define INFO_LOG(...) LOG(nullptr, __VA_ARGS__)
#endif

namespace Debug::Logger
{
	template<typename TYPE, typename ... TYPES>
	void LogArray(TYPE arg) {
		std::cout << arg;
	}
	
	template<typename TYPE, typename ... TYPES>
	void LogArray(TYPE arg, TYPES&... rest) {
		std::cout << arg;
		LogArray(rest...);
	}


	template<typename TYPE>
	void Info(const char* category, TYPE value)
	{
		if (category == nullptr)
		{
			LogArray("", value);
		}
		else
		{
			std::cout << "[" << category;
			LogArray("]: ", value);
		}

		std::cout << std::endl;
	}
	
	template<typename ... TYPES>
	void Info(const char* category, TYPES&... values)
	{
		if (category == nullptr)
		{
			LogArray("", values...);
		}
		else
		{
			std::cout << "[" << category;
			LogArray("]: ", values...);
		}

		std::cout << std::endl;
	}
}