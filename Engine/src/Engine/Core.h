#pragma once

#include "Engine/DataTypes/Primitives.h"
#include "Engine/DataTypes/IObject.h"
#include "Engine/DataTypes/Object.h"

#include "Engine/DataTypes/Collections/String.h"

#include "Engine/DataTypes/Iterators/IIterator.h"
#include "Engine/DataTypes/Iterators/ForwardIterator.h"

#include "Engine/DataTypes/Collections/Array.h"
#include "Engine/DataTypes/Collections/List.h"

namespace Engine
{
	// Primitives
	using Byte = Engine::DataTypes::Byte;
	using ushort = Engine::DataTypes::ushort;
	using uint = Engine::DataTypes::uint;
	using Long = Engine::DataTypes::Long;
	using Ulong = Engine::DataTypes::Ulong;

	using SizeT = Engine::DataTypes::SizeT;

	// Object
	using IObject = Engine::DataTypes::IObject;
	using Object = Engine::DataTypes::Object;

	// Iterators
	template <typename Iterator_T>
	using IIterator = Engine::DataTypes::Iterators::IIterator<Iterator_T>;
	template <typename T, typename Iterator_T>
	using Iterator = Engine::DataTypes::Iterators::ForwardIterator<T, Iterator_T>;
	
	// Collections
	template <typename T, const SizeT LENGTH>
	using Array = Engine::DataTypes::Collections::Array<T, LENGTH>;
	template <typename T>
	using List = Engine::DataTypes::Collections::List<T>;
	
	using String = Engine::DataTypes::Collections::String;
}

// TODO Delete this
#include <iostream>
#define TEMP_LOG(OBJ) std::cout << OBJ << std::endl;
#define TEMP_ERROR(OBJ) std::cout << "ERROR: " << OBJ << std::endl;