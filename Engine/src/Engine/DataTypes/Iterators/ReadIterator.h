#pragma once

#include "Engine/Core.h"

#include "Engine/DataTypes/Iterators/BasicIterator.h"

namespace Engine::DataTypes::Iterators
{
	template<typename T, typename Iterator_T>
	class ReadIterator : public BasicIterator<T, Iterator_T>
	{
		// Virtual functions
	public:
		//virtual const T& operator*() = 0;
	};
}
