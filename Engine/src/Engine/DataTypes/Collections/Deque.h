#pragma once

#include "Engine/Core.h"
#include "Engine/DataTypes/Collections/BasicDeque.h"

namespace Engine::DataTypes::Collections
{
	template <typename T>
	class Deque : public BasicDeque<T>
	{

	};
}