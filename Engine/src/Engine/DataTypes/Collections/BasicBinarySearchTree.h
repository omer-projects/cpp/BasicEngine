#pragma once

#include "Engine/Core.h"

#include "Engine/DataTypes/Collections/BasicBinaryTree.h"

namespace Engine::DataTypes::Collections
{
	template <typename T>
	class BasicBinarySearchTree : public BasicBinaryTree<T>
	{
		// Sub-Classes
		using BasicNode = BasicBinaryTree<T>::BasicNode;

		// Actions
	public:
		void Push(const T& value)
		{
			if (!this->HasNodes())
			{
				this->m_TopNode = new BasicNode(value);
			}
			else
			{
				BasicNode* node = this->m_TopNode;
				while (true)
				{
					if (value < node->m_Value)
					{
						if (node->m_Left != nullptr)
						{
							node = node->m_Left;
						}
						else
						{
							node->m_Left = new BasicNode(value, node);
							break;
						}
					}
					else
					{
						if (node->m_Right != nullptr)
						{
							node = node->m_Right;
						}
						else
						{
							node->m_Right = new BasicNode(value, node);
							break;
						}
					}
				}
			}
		}

		//void Push(T&& value);

		// Tests
	public:
		void Print()
		{
			PrintNode(this->m_TopNode);
		}

	private:
		void PrintNode(BasicNode* node)
		{
			if (node != nullptr)
			{
				TEMP_LOG(node->m_Value);
				PrintNode(node->m_Left);
				PrintNode(node->m_Right);
			}
		}
	};
}